export class DSU {
  id: number = -1;
  name: string = "";
  description: string = "";
  cert: number = 0;
  errorMessage: string = "none";

  constructor(id?: number, name?: string, description?: string, cert?: number) {

    if(id || id==0) this.id = id;
    if(name) this.name = name;
    if(description) this.description = description;
    if(cert || cert==0) this.cert = cert;
  }
}
