import { DSU } from './ee-dsu';

export class Site {
  name: string = "";
  description: string = "";
  dsu_id: number = -1;
  errorMessage: string = "none";

  constructor(name?: string, description?: string, dsu_id?: number) {
    if(name) this.name = name;
    if(description) this.description = description;
    if(dsu_id || dsu_id==0) this.dsu_id = dsu_id;
  }
}
