import { Component, OnInit, Input } from '@angular/core';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { DSU } from '../../models/ee-dsu';
import { Site } from '../../models/ee-site';
import { SiteFormComponent } from '../ee-site-form/ee-site-form.component';
import { DsuApiService } from '../../services/ee-dsu-api/ee-dsu-api.service';
import {SaveMonitorService} from '../../services/ee-save-monitor/ee-save-monitor.service';

@Component({
  selector: 'ee-add-site',
  templateUrl: './ee-add-site.component.html',
  styleUrls: ['./ee-add-site.component.scss']
})
export class AddSiteComponent implements OnInit {
  private readonly SUCCESS_MESSAGE = "Forms saved!";
  private readonly ERROR_MESSAGE = "There was a problem submitting some of your forms. Review them below.";
  private readonly SERVER_DOWN_MESSAGE = "Server didn't respond. Try again later or contact your network administrator.";

  private _saving: boolean = false;
  private _allowSaving: boolean = true;
  private _errorMessage: string = "";
  private _storedDsus: Array<DSU> = [];
  private _loading: boolean = true;
  private _currentSites: Array<Site> = [];

  private _statusMessage: string = "none";
  private _forms: Array<HTMLElement> = [];

  constructor(private _api: DsuApiService, private _saveMonitor: SaveMonitorService) {
  }

  ngOnInit() {
    this._loading = true;
    this._statusMessage = "none";
    this.clearCurrentSites();
    this._saveMonitor.setUnsavedChecker(this._currentSites, this.hasUnsavedData);

    var that = this;
    this._api.getDsus(function(results: Object) {
      that._storedDsus=[];

      if(typeof results == "object" && Object.keys(results).length > 0) {
        for(var dsu in results) {
          that._storedDsus.push(new DSU(results[dsu].id, results[dsu].name, results[dsu].description, results[dsu].cert));
        }
      }
      else {
        that.disableSaving("You have to add some DSUs before you can add sites");
      }
      that._loading = false;
    },
    function(err) {
      that._loading = false;
      that.disableSaving("Couldn't contact the server. Try again later or contact your administrator.");
    });
  }


    hasUnsavedData(sites: Array<Site>) {
      for(var i = 0; i < sites.length; i++) {
        if(sites[i].name != "") return true;
        if(sites[i].description != "") return true;
        if(sites[i].dsu_id != -1) return true;
      }
      return false;
    }

  disableSaving(message:string) {
    this._allowSaving = false;
    this._errorMessage = message;
  }

  addNewSite() {
    this._currentSites.push(new Site());
  }

  clearCurrentSites() {
    this._statusMessage = "none";
    this._currentSites = [new Site()];
    this._saveMonitor.setUnsavedChecker(this._currentSites, this.hasUnsavedData);
  }

  saveCurrentSites() {
    for(let i = (this._currentSites.length-1); i >= 0; i--) {
      if(this._currentSites[i].name == "" && this._currentSites[i].description == "" && this._currentSites[i].dsu_id == -1) {
        this._currentSites.splice(i, 1);
      }
    }

    if(this._currentSites.length > 0) {
      if(!this._saving){//Don't try to save until last save completes. Stops user spamming save button
        this._saving = true;
        var that = this;
        this._api.addSites(this._currentSites,
          function() {//on finish all success
          that._saving = false;
          that._saveMonitor.setUnsavedChecker(that._currentSites, that.hasUnsavedData);
          that.clearCurrentSites();
          that._statusMessage = that.SUCCESS_MESSAGE;
        },
        function() {//on finish all failure
          that._saving = false;
          that._saveMonitor.setUnsavedChecker(that._currentSites, that.hasUnsavedData);
          that._statusMessage = that.ERROR_MESSAGE;
          for(var i =(that._currentSites.length-1); i >= 0; i--) {
            if(that._currentSites[i].errorMessage == "none") {//Remove any entires with no error
              that._currentSites.splice(i, 1);
            }
          }
        },
        function(data, submissionIndex) {//on submit each success
        },
        function(data, submissionIndex) {//on submit each failure
          if(typeof data._body == "string") {
            that._currentSites[submissionIndex].errorMessage = data._body;
          } else {
            that._currentSites[submissionIndex].errorMessage = that.SERVER_DOWN_MESSAGE;
          }
        });
      }
      else {
        this.clearCurrentSites();
      }
    }
  }

    remove(site) {
      var index = this._currentSites.indexOf(site, 0);
      if (index > -1) {
         this._currentSites.splice(index, 1);
      }
    }
}
