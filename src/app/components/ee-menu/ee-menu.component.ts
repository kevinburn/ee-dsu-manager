import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {SaveMonitorService} from '../../services/ee-save-monitor/ee-save-monitor.service';
@Component({
  selector: 'ee-menu',
  templateUrl: './ee-menu.component.html',
  styleUrls: ['./ee-menu.component.scss']
})
export class MenuComponent implements OnInit {


  private _items: Array<Object> = [{name:"Home", routerLink:"home"},
    {name:"Add DSU", routerLink:"add-dsu"},
    {name:"Add Site", routerLink:"add-site"}
  ]
  private _activeItem: Object = null;//this._items[0];
  private _unsavedData: Array<Object> = [];
  private _unsavedChecker: Function = null;

  constructor(private router:Router, private _saveMonitor: SaveMonitorService) {
    var that = this;
    this.setActiveItemFromRoute();
  }

  setActiveItemFromRoute() {
    let routerLink = window.location.pathname.replace('/','');
    if(routerLink == '') this._activeItem = this._items[0];
    else {
      for(var i = 0; i < this._items.length; i++) {
        if(this._items[i]["routerLink"] == routerLink) {
          this._activeItem = this._items[i];
        }
      }
    }
  }

  ngOnInit() {
  }

  ngAfterContentInit() {

  }

  updateActiveItem(item) {
    
    if(this._saveMonitor.hasUnsavedChanges(this._unsavedData, this._unsavedChecker)
       && this._activeItem["routerLink"] != item["routerLink"]){

      var ret = window.confirm("You will lose any unsaved changes. Are you sure?");
      if (ret == true) {
          this.changeActiveItem(item);
          this._saveMonitor.discardSavedChanges();
      }
    }
    else {
      this.changeActiveItem(item);
      this._saveMonitor.discardSavedChanges();
    }
  }

  changeActiveItem(item) {
    this.router.navigate([item.routerLink]);
    this._activeItem = item;
  }




}
