import { Component, OnInit } from '@angular/core';
import { DSU } from '../../models/ee-dsu';
import { DsuApiService } from '../../services/ee-dsu-api/ee-dsu-api.service';
import {SaveMonitorService} from '../../services/ee-save-monitor/ee-save-monitor.service';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import {AddSiteComponent} from '../ee-add-site/ee-add-site.component';
@Component({
  selector: 'ee-add-dsu',
  templateUrl: './ee-add-dsu.component.html',
  styleUrls: ['./ee-add-dsu.component.scss']
})
export class AddDsuComponent implements OnInit{
  private readonly SUCCESS_MESSAGE = "Forms saved!";
  private readonly ERROR_MESSAGE = "There was a problem submitting some of your forms. Review them below.";
  private readonly SERVER_DOWN_MESSAGE = "Server didn't respond. Try again later or contact your network administrator.";

  private _saving: boolean = false;
  private _currentDsus: Array<DSU> = [];
  private _statusMessage: string = "none";
  private _forms: Array<HTMLElement> = [];
  constructor(private _api: DsuApiService, private _saveMonitor: SaveMonitorService) {
  }

  ngOnInit() {
    this._statusMessage = "none";
    this.clearCurrentDsus();
    this._saveMonitor.setUnsavedChecker(this._currentDsus, this.hasUnsavedData);
  }

  hasUnsavedData(dsus: Array<DSU>) {
    for(var i = 0; i < dsus.length; i++) {
      if(dsus[i].name != "") return true;
      if(dsus[i].description != "") return true;
      if(dsus[i].cert != 0) return true;
    }
    return false;
  }

  addNewDsu() {
    this._currentDsus.push(new DSU());
  }

  clearCurrentDsus() {
    this._statusMessage = "none";
    this._currentDsus = [new DSU()];
    this._saveMonitor.setUnsavedChecker(this._currentDsus, this.hasUnsavedData);
  }

  saveCurrentDsus() {
    for(let i = (this._currentDsus.length-1); i >= 0; i--) {
      if(this._currentDsus[i].name == "" && this._currentDsus[i].description == "" && this._currentDsus[i].cert == 0) {
        this._currentDsus.splice(i, 1);
      }
    }
    if(this._currentDsus.length > 0) {
      if(!this._saving){//Don't try to save until last save completes. Stops user spamming save button
        this._saving = true;
        var that = this;
        this._api.addDsus(this._currentDsus,
          function() {//on finish all success
          that._saving = false;
          that._saveMonitor.setUnsavedChecker(that._currentDsus, that.hasUnsavedData);
          that.clearCurrentDsus();
          that._statusMessage = that.SUCCESS_MESSAGE;
        },
        function() {//on finish all failure
          that._saving = false;
          that._saveMonitor.setUnsavedChecker(that._currentDsus, that.hasUnsavedData);
          that._statusMessage = that.ERROR_MESSAGE;
          for(var i =(that._currentDsus.length-1); i >= 0; i--) {
            if(that._currentDsus[i].errorMessage == "none") {//Remove any entires with no error
              that._currentDsus.splice(i, 1);
            }
          }
        },
        function(data, submissionIndex) {//on submit each success
        },
        function(data, submissionIndex) {//on submit each failure
          if(typeof data._body == "string") {
            that._currentDsus[submissionIndex].errorMessage = data._body;
          } else {
            that._currentDsus[submissionIndex].errorMessage = that.SERVER_DOWN_MESSAGE;
          }
        });
      }
    }
    else {
      this.clearCurrentDsus();
    }
  }

  remove(dsu) {
    var index = this._currentDsus.indexOf(dsu, 0);
    if (index > -1) {
       this._currentDsus.splice(index, 1);
    }
  }

}
