import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsuFormComponent } from './ee-dsu-form.component';

describe('DsuFormComponent', () => {
  let component: DsuFormComponent;
  let fixture: ComponentFixture<DsuFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsuFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsuFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
