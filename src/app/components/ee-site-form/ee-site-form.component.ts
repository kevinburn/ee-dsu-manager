import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Site } from '../../models/ee-site';
import { DSU } from '../../models/ee-dsu';

@Component({
  selector: 'ee-site-form',
  templateUrl: './ee-site-form.component.html',
  styleUrls: ['./ee-site-form.component.scss']
})
export class SiteFormComponent implements OnInit {
  private _site: Site = null;
  private _storedDsus: Array<DSU> = null;
  @Output() onDelete = new EventEmitter();


  @Input('site')
  set site(value: Site) {
    this._site = value;
  }

  @Input('stored-dsus')
  set storedDsus(value: Array<DSU>) {
    this._storedDsus = value;
  }

  constructor() { }

  ngOnInit() {
  }

}
