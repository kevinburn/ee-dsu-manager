import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/ee-header/ee-header.component';
import { MenuComponent } from './components/ee-menu/ee-menu.component';
import { SiteFormComponent } from './components/ee-site-form/ee-site-form.component';
import { DsuFormComponent } from './components/ee-dsu-form/ee-dsu-form.component';
import { HomeComponent } from './components/ee-home/ee-home.component';
import { AddDsuComponent } from './components/ee-add-dsu/ee-add-dsu.component';
import { AddSiteComponent } from './components/ee-add-site/ee-add-site.component';
import {SaveMonitorService} from './services/ee-save-monitor/ee-save-monitor.service';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    SiteFormComponent,
    DsuFormComponent,
    HomeComponent,
    AddDsuComponent,
    AddSiteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [SaveMonitorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
