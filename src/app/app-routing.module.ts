import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/ee-home/ee-home.component';
import { AddDsuComponent } from './components/ee-add-dsu/ee-add-dsu.component';
import { AddSiteComponent } from './components/ee-add-site/ee-add-site.component';

const routes: Routes = [
  { path: '', component: HomeComponent  },
  { path: 'home', component: HomeComponent },
  { path: 'add-dsu', component: AddDsuComponent},
  { path: 'add-site', component: AddSiteComponent}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
