import { Injectable } from '@angular/core';
//import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin'
import {DSU} from '../../models/ee-dsu';
import {Site} from '../../models/ee-site';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DsuApiService {
  public readonly TARGETS: Object = { sites: 1, dsus: 2};
  public cat: string = "cattt";
  private _siteAddress = "http://localhost:3000/v1/api/sites";
  private _dsuAddress = "http://localhost:3000/v1/api/dsus";

  private _httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
  };

  constructor(private http: Http) {
  }

  /**
  * Store our DSUs using the api
  * @param dsus The array of dsus we want to add
  * @param onSuccess What to do on successful upload of all dsus. (passed http response as a parameter)
  * @param onError What to do on failed upload of at least one dsu. (passed http response as a parameter)
  * @param onEachSuccess What to do each time a dsu uploads (passed http response and successfully uploaded dsu as a parameter)
  * @param onEachError What to do each time a dsu fails to upload (passed http error and failed dsu as a parameter)
  */
  addDsus(dsus: Array<DSU>, onSuccess: Function, onError: Function, onEachSuccess: Function, onEachError: Function) {
    var submissions = [];
    for(let i =0; i < dsus.length; i++) {
      submissions.push({name:dsus[i].name, description:dsus[i].description, cert:dsus[i].cert});
    }
    this._add(this._dsuAddress, submissions, onSuccess, onError, onEachSuccess, onEachError);
  }

  addSites(sites: Array<Site>, onSuccess: Function, onError: Function, onEachSuccess: Function, onEachError: Function) {
    var submissions = [];
    for(let i =0; i < sites.length; i++) {
      submissions.push({name:sites[i].name, description:sites[i].description, dsuId:sites[i].dsu_id});
    }
    this._add(this._siteAddress, submissions, onSuccess, onError, onEachSuccess, onEachError);
  }


  private _add(address: string, submissions: Array<Object>, onSuccess: Function, onError: Function, onEachSuccess: Function, onEachError: Function) {
    let requestsCompleted = 0;
    let requests: Array<Observable<any>> = []
    let failed = false;
    for(let i =0; i < submissions.length; i++) {
      var req = this.http.put(address, submissions[i], this._httpOptions);
      requests.push(req);
      req.subscribe(
        data => {
          if(typeof onEachSuccess == "function") onEachSuccess(data, i);
          requestsCompleted++;
          if(requestsCompleted==submissions.length) {
            if(typeof onError == "function" && failed) onError();
            else if (typeof onSuccess == "function") onSuccess();
          }
        },
        err => {
          if(typeof onEachError == "function") {
            onEachError(err, i);
            failed = true;
          }
          requestsCompleted++;
          if(requestsCompleted==submissions.length) {
            if(typeof onError == "function" && failed) onError();
            else if (typeof onSuccess == "function") onSuccess();
          }
        },
        () => {     }
      )
    }
  }

  /**
  *
  */
  getDsus(onSuccess: Function, onError: Function) {
    this.http.get(this._dsuAddress)
      .subscribe(
        data => { if(data.status == 200 && typeof onSuccess == "function") onSuccess(data.json())},
        err => {if(typeof onError == "function") onError(err)},
        () => {}
    );
  }



}
