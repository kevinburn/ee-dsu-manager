import { TestBed, inject } from '@angular/core/testing';

import { EeDsuApiService } from './ee-dsu-api.service';

describe('EeDsuApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EeDsuApiService]
    });
  });

  it('should be created', inject([EeDsuApiService], (service: EeDsuApiService) => {
    expect(service).toBeTruthy();
  }));
});
