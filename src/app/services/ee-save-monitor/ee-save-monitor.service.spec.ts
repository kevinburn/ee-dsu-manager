import { TestBed, inject } from '@angular/core/testing';

import { EeSaveMonitorService } from './ee-save-monitor.service';

describe('EeSaveMonitorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EeSaveMonitorService]
    });
  });

  it('should be created', inject([EeSaveMonitorService], (service: EeSaveMonitorService) => {
    expect(service).toBeTruthy();
  }));
});
