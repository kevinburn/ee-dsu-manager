import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/**
* Tells router if we have unsaved information
*/
@Injectable({
  providedIn: 'root'
})
export class SaveMonitorService {
  private unsavedData: Array<Object> = [];
  private savedChecker: Function = null;

  constructor() {
}

  public hasUnsavedChanges(unsavedData, checker) {
    if(this.savedChecker) {
      return this.savedChecker(this.unsavedData)
    }
    return false;
  }

  public discardSavedChanges() {
    this.unsavedData = [];
    this.savedChecker = null;
  }


  public setUnsavedChecker(obj:Array<Object>, savedChecker: Function) {
    this.unsavedData = obj;
    this.savedChecker = savedChecker;
  }

}
