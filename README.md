# Front end development challenge
This is Kevin Beirne's submission for the front end development challenge issued by Paul McCarthy. A copy of the challenge spec is included in this repository.


## Technology
- This app was built using Angular 6 and written in typescript
- This app uses SASS
- This app will work on any browser supporting ES6 (Angular can be polyfilled to work with older browser versions if necessary)
- This app was primarily tested on the latest version of Chrome
- This app works on any screen size above 300pixels wide


## Installation
- Install the latest version of node (8.11.3 at time of writing.)
- Open the front-end-api spec referenced in this challenge and run:
``npm install``
``npm start``
- Clone this repository
- Run:
``npm install``
``npm start``
- Navigate to localhost:4200 to see the application running

## Navigating
If you are not too familiar with angular and want to review my code then the best place to start is in **src/app** and then navigate to the **/components**, **/models** and **/services** folders. These house the majority of the work.

-   If you want to see my css you'll find component specific styles in its folder e.g. **app/components/ee-header/ee-header.component.scss**
- The universal styles folder can be found in **src/sass/styles.scss**
- The majority of the form's scss can be found in **src/sass/ee-forms.scss** (as it is shared between multiple components)

## Assumptions
- This application is not meant to display results after they've been added via the Rest API
- The user must manually save their forms

## Other notes
- I wrote this in angular and typescript as I wrote the other challenge in vanilla javascript and wanted to show you the broadest array of skills that I could.
- Despite the allowance of CORS on your front-end-api I was still having CORS issues witth the front-end-api on firefox when running it from the same origin.
- There is some similar code in the ee-add-site and ee-add-dsu components. In an enterprise application some of this could be abstracted into parent components, interfaces and shared data models.
- Additional error checking, browser support and testing would be present in an enterprise level version of this application.
